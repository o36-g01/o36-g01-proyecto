Command line instructions

You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "Daniela Leyton" <br>
git config --global user.email "danielaleytongiraldo@gmail.com"

Create a new repository

git clone https://gitlab.com/o36-g01/o36-g01-proyecto.git <br>
cd o36-g01-proyecto <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main

Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/o36-g01/o36-g01-proyecto.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main

Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/o36-g01/o36-g01-proyecto.git <br>
git push -u origin --all <br>
git push -u origin --tags
